## How to build, install and use the project

1. Create an account on BitBucket
	1.1 Go to https://www.bitbucket.org/
	1.2 Go to "Sign-up" page
	1.3 Fill in the information
	1.4 Select "Free Plan"
	1.5 Type the text in the Captcha
	1.6 Click "Sign up"
2. Create a Repository
	2.1 From Bitbucket, click the  +  icon in the global sidebar and select Repository.
	2.2 Enter repository name, access level (private or public) include a README, and set the version control to Git.
3. Adding files or folder in repository
	3.1 Clone the repository in the local system
	3.2 Select  sourcetree
	3.3 Choose a local destination and create a folder 
	3.4 Set a filename or leave the defualt filename
	3.5 Click add or create button
	3.6 Copy and paste any file or folder in the local drive
	3.7 Select all the files or folder and click "Push" in the menu
	3.8 Write a description below and click commit button
4. Go back to the repository created
	4.1 Click source to check the added files or folder in the repository
5. Use the project
	5.1 Download or clone the repository locally
	5.2 Download VS Code
	5.3 Open the project folder in the VS Code
	5.4 Run the code by pressing F5 key
	5.5 Edit the code for any improvement
	5.5 Save the project
	5.6 Repeat Step 5.4

## Why I choose MIT License
The MIT license gives users express permission to reuse code for any purpose, sometimes even if code is part of 
proprietary software. As long as users include the original copy of the MIT license in their distribution, they can 
make any changes or modifications to the code to suit their own needs.
